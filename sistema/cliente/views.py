from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import FormView, View, TemplateView
from django.http import HttpResponse
from django.views.generic import CreateView, ListView, UpdateView, FormView, DetailView, TemplateView

from .forms import InsereClienteForm
from .models import clientes


class IndexView(TemplateView):
    template_name = "index.html"

class ClienteCreateView(CreateView):
  template_name = "cadastrar.html"
  model = clientes
  form_class = InsereClienteForm
  success_url = reverse_lazy("cliente:index")
