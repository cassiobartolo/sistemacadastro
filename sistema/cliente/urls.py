from django.urls import path
from . import views
from django.conf import settings



app_name = 'cliente'

# urlpatterns a lista de roteamentos de URLs para funções/Views
urlpatterns = [
    path("", views.IndexView.as_view(), name="index",),
    path("cadastrar", views.ClienteCreateView.as_view(), name='cadastrar'),
]