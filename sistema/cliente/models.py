from django.db import models
from  django.utils.timezone import timezone

class clientes(models.Model):

  nome = models.CharField(
    max_length=255,
    null=False,
    blank=False,
    verbose_name="Nome"
  )

  cpf = models.CharField(
    max_length=14,
    null=False,
    blank=False,
    verbose_name="CPF"
  )
  
  nome_mae = models.CharField(
    max_length=255,
    null=False,
    blank=False,
    verbose_name="Nome Mãe"
  )

  rg = models.IntegerField(
    default=11,
    null=False,
    blank=False,
    verbose_name="RG"
  )

  telefone = models.IntegerField(
    null=False,
    blank=False,
    verbose_name="Telefone"
  )
  
  rua = models.CharField(
    max_length=255,
    null=False,
    blank=False,
    verbose_name="Rua"
  )
  
  bairro = models.CharField(
    max_length=255,
    null=False,
    blank=False,
    verbose_name="Bairro"
  )
  
  cidade = models.CharField(
    max_length=255,
    null=False,
    blank=False,
    verbose_name="Cidade"
  )
  
  estado = models.CharField(
    max_length=255,
    null=False,
    blank=False,
    verbose_name="Estado"
  )
  
  numero = models.IntegerField(
    null=False,
    blank=False,
    verbose_name="Numero Residencia"
  )

  estado_civil = models.CharField(
    max_length=255,
    null=False,
    blank=False,
    verbose_name="Estado Civil"
  )

  objetos = models.Manager()