from django.urls.conf import include
from django.contrib import admin
from django.urls import path

app_name = "app"

urlpatterns = [
    # URL padrão
    path('', include('cliente.urls')),

    # Interface administrativa
    path('admin/', admin.site.urls),
]